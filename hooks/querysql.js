import Axios from "axios";
import moment from "moment";
// const REQUEST_URL = "http://10.32.0.84:7015/api";
const REQUEST_URL = "http://112.121.138.46/dealer_api/api";
// const REQUEST_URL = "http://localhost:7015/api";

export const qureyDataThailand = async (idPro, idAmp) => {
  const res = await Axios.get(REQUEST_URL + `/register`, {
    params: {
      idPro,
      idAmp,
    },
  });
  return res.data;
};

export const insertData = async (value) => {
  console.log(value)
  const res = await Axios.post(REQUEST_URL + `/register/insertCustomers`, {
    value: value,
  });
  return res.data;
};

export const upload = async (data, dataId) => {
  const res = await Axios.post(REQUEST_URL + `/register/uploadImg`, data, {
    headers: {
      id: dataId.userId + ":" + dataId.refId,
    },
  });
  return res.data;
};

export const login = async (value) => {
  const res = await Axios.post(REQUEST_URL + `/login/`, { value });
  return res.data;
};

export const sendMail = async (receiver, subject, body) => {
  const res = await Axios.post(
    `http://10.32.0.86:81/onlinewholesale_backend/api/sendmail`,
    {
      receiver,
      subject,
      body,
    }
  );
  return res.data;
};

export const getDoc = async (email) => {
  const res = await Axios.post(REQUEST_URL + `/login/getdoc`, { email });
  return res.data;
};
export const products = async () => {
  const res = await Axios.get(REQUEST_URL + `/products`);
  return res.data;
};

export const productsDetail = async (code) => {
  const res = await Axios.post(REQUEST_URL + `/products/detail`, { code });

  return res.data;
};

// export const imgmoc = async () => {
//   const res = await Axios.get(REQUEST_URL+ `/products/img/`)
//   return res.data;
// }