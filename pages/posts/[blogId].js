import React, { useEffect, useState } from "react";
import { productsDetail, products } from "../../hooks/querysql";
import { Container, Row, Col } from "reactstrap";
import Slider from "react-slick";
function Blog({ data, path }) {
  const initialState = {
    color: [],
  };
  const [state, setstate] = useState(initialState);
  console.log(data.data[0]);

  return (
    <Row className="animate__animated animate__fadeIn">
      <Col md="6">as</Col>
      <Col md="6">b</Col>
    </Row>
  );
}

export const getStaticPaths = async () => {
  const product = await products();
  const paths = product.items.map((product) => ({
    params: { blogId: product.prodcode },
  }));
  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps = async ({ params }) => {
  const products = await productsDetail(params.blogId);

  const data = await products;

  return {
    props: { data },
  };
};

export default Blog;
