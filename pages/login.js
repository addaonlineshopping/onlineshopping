import React, { useState } from "react";
import Link from "next/link";
import Verify from "../Components/verify";
import { useRouter } from "next/router";
import { Button, Form, Input, message } from "antd";
import { login } from "../hooks/querysql";

function Login() {
  const initialState = {
    visible: false,
    email: "",
  };
  const [state, setState] = useState(initialState);

  const [form] = Form.useForm();
  const router = useRouter();

  const _onSubmit = async (fieldsValue) => {
    const res = await login(fieldsValue);
    if (res.success && res.status && res.allowed) {
      console.log(res); //set cookie
      router.push("/product");
    } else if (res.success && res.status && !res.allowed) {
      openMessage();
    } else if (res.success && !res.status) {
      error(res.message);
    }
  };

  const key = "updatable";

  const openMessage = () => {
    message.loading({
      content: "E-mail ของคุณยังไม่ได้รับการอนุมัติ! กำลังโหลดข้อมูล...",
      key,
      duration: 2,
    });

    setTimeout(() => {
      setState({
        visible: true,
        email: form.getFieldValue(["email"]),
      });
    }, 2000);
  };

  const error = (msg) => {
    message.error(`${msg}`);
  };

  const showModal = () => {
    // this.setState({
    //   visible: true,
    // });
  };

  const handleOk = (e) => {
    // console.log(e);
    // this.setState({
    //   visible: false,
    // });
  };

  const handleCancel = (e) => {
    setState({
      visible: false,
      email: "",
    });
  };

  return (
    <>
      <Verify {...state} handleCancel={handleCancel} />
      <div className="card-authentication">
        <img
          // src={`http://112.121.138.46/dealer_public/images/e-commerce.png`}
          src={`/images/e-commerce.png`}
          alt="e-commerce icon"
          className="e-commerce-logo"
        />
        <h3 className="mt-2 text-center">ยินดีตอนรับ</h3>
        <small className="mb-2 text-center d-block">
          โปรดระบุ อีเมลเเละรหัสผ่าน
        </small>
        <hr />
        <Form form={form} onFinish={_onSubmit}>
          <Form.Item
            name="email"
            rules={[
              {
                required: true,
                type: "email",
                message: "กรอกอีเมล! xxx@email.com",
              },
            ]}
          >
            <Input
              size="large"
              // className="text-center"
              placeholder="Email"
              maxLength={50}
              allowClear={true}
            />
          </Form.Item>
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: "กรอกรหัสผ่าน!",
              },
            ]}
          >
            <Input.Password
              size="large"
              // className="text-center password-pad"
              placeholder="Password"
              autoComplete="off"
              maxLength={50}
            />
          </Form.Item>
          <hr />
          <Button
            className="btn-login"
            size="large"
            htmlType="submit"
            style={{ width: "100%" }}
          >
            Sign In
          </Button>
          <Link href={"/register"}>
            <small className="sub-text-authentication" style={{ fontSize: 15 }}>
              สมัครสมาชิคใหม่ !
            </small>
          </Link>
        </Form>
      </div>
    </>
  );
}

export default Login;
