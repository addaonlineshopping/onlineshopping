import React, { useState, useEffect } from "react";
import Link from "next/link";
import { products } from "../hooks/querysql";
import { Tooltip } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import {
  Row,
  ListGroup,
  ListGroupItem,
  Pagination,
  PaginationItem,
  PaginationLink,
} from "reactstrap";
import alaSql from "alasql";

// const goToProduct = (item) => {
//   console.log(item)
// }

const ShopRender = ({ item, path }) => {
  return (
    <Link href={`/posts/[id]`} as={`/posts/${item.prodcode}`}>
    <div
      className="product m-1"
    >
      <img
        className="product-img"
        src={path + item.thumbnail}
        alt="Image Not Found"
      />
      <div className="text-left p-2">
        <div className="d-flex align-items-center mb-2">
          <b>Product: </b>
          <Tooltip title={item.title}>
            <label className="product-desc mb-0 ml-1">{item.title}</label>
          </Tooltip>
        </div>
        <div className="d-flex align-items-center mb-2">
          <b>type: </b>
          <label className="product-desc mb-0 ml-1">{item.name}</label>
        </div>
        <div>
          <b>Price:</b>
          <label className="ml-1">{item.p_novat}</label>
        </div>
        <div>
          <b>Price:</b>
          <label className="ml-1">{item.price}</label>
        </div>
      </div>
    </div>
    </Link>
  );
};

const Product = (props) => {
  const initialState = {
    data: [],
    dataFatch: [],
    num: 1,
  };
  const [state, setstate] = useState(initialState);

  const fatchPost = (page) => {
    let pagPost = null;
    let offset = 0;
    const fatch = 12;
    if (!page || page === 1) {
      pagPost = alaSql(
        `SELECT * FROM ? OFFSET ${offset} ROWS FETCH NEXT ${fatch} ROWS ONLY`,
        [post.items]
      );
    } else {
      offset = 12 * page - 12;
      pagPost = alaSql(
        `SELECT * FROM ? OFFSET ${offset} ROWS FETCH NEXT ${fatch} ROWS ONLY`,
        [post.items]
      );
    }

    setstate({
      ...state,
      num: page ? page : 1,
      data: post.items,
      dataFatch: pagPost,
    });
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  useEffect(() => {
    if (post.items) {
      fatchPost();
    }
  }, []);

  const PagList = () => {
    const list = [];
    for (let index = 0; index < post.count; index++) {
      list.push(
        <PaginationItem active={num === index + 1 ? true : false} key={index}>
          <PaginationLink onClick={() => fatchPost(index + 1)}>
            {index + 1}
          </PaginationLink>
        </PaginationItem>
      );
    }
    return list;
  };

  const { data, dataFatch, num } = state;
  const { post } = props;

  return (
    <Row>
      <div className="shop-menu">
        <ListGroup>
          <ListGroupItem>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <SearchOutlined style={{ fontSize: 24, marginRight: 10 }} />
              <h5 className="mb-0">ค้นหาปรเภทสินค้า</h5>
            </div>
          </ListGroupItem>
          <ListGroupItem className="text-center list-group-hover">
            รายการทั้งหมด
          </ListGroupItem>
          {post.type &&
            post.type.map((item, index) => {
              return (
                <ListGroupItem
                  className="text-center list-group-hover"
                  key={index}
                >
                  {item.name}
                </ListGroupItem>
              );
            })}
        </ListGroup>
      </div>

      <div className="shop animate__animated animate__fadeIn">
        {dataFatch &&
          dataFatch.map((item, index) => {
            return (
              <div key={index}>
                <ShopRender item={item} path={props.path} />
              </div>
            );
          })}
        <Pagination className="mt-3">
          <PaginationItem>
            <PaginationLink first href="#" />
          </PaginationItem>
          <PaginationItem>
            <PaginationLink previous href="#" />
          </PaginationItem>
          <PagList />
          <PaginationItem>
            <PaginationLink next href="#" />
          </PaginationItem>
          <PaginationItem>
            <PaginationLink last href="#" />
          </PaginationItem>
        </Pagination>
      </div>
    </Row>
  );
};

export const getStaticProps = async () => {
  const res = await products();
  const post = await {
    type: res.type,
    items: res.items,
    count: res.items.length / 12,
  };
  return {
    props: { post },
  };
};

export default Product;
