import "bootstrap/dist/css/bootstrap.min.css";
import "antd/dist/antd.css";
import "slick-carousel/slick/slick-theme.css";
import "slick-carousel/slick/slick.css";
import "../node_modules/animate.css";
import "../css/style.css";
import PageLayout from "../Components/layout/layout";
const path = "http://10.32.0.14:83/thumbnail/";
// const isProd = process.env.NODE_ENV === 'production'

function MyApp({ Component, pageProps }) {
  return (
    <PageLayout>
      <Component {...pageProps} path={path} />
    </PageLayout>
  );
}

export default MyApp;
