import React, { useState, useRef } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { Container, Row, Col } from "reactstrap";

import { Button, notification, message as msg, BackTop } from "antd";
import {
  qureyDataThailand,
  insertData,
  upload,
  sendMail,
} from "../hooks/querysql";
const ComponentFormdata = dynamic(() =>
  import("../Components/registers/formdata")
);
const ComponentFormdoc = dynamic(() =>
  import("../Components/registers/formdoc")
);

const _ = undefined;
function Register(props) {
  const router = useRouter();
  const fromRef = useRef();
  const fromRef2 = useRef();
  const doc1Ref = useRef();
  const doc2Ref = useRef();
  const doc3Ref = useRef();
  const doc4Ref = useRef();
  const initialState = {
    dataAmphures: [],
    dataDistricts: [],
    dataFile: [],
    zipcode: "",
    message: null,
    status: null,
    inputemail: "",
  };
  const [state, setstate] = useState(initialState);
  const reciveProvinces = async (id) => {
    const res = await qureyDataThailand(id, _);
    const data = await res.data.map((value, index) => {
      return {
        id: value.id,
        code: value.code,
        value: value.name_th.replace("*", ""),
      };
    });

    setstate({
      ...state,
      dataAmphures: data,
    });
  };

  const reciveAmpures = async (id) => {
    const res = await qureyDataThailand(_, id);
    const data = await res.data.map((value, index) => {
      return {
        id: value.id,
        zipcode: value.zip_code,
        value: value.name_th.replace("*", ""),
      };
    });
    setstate({
      ...state,
      dataDistricts: data,
    });
  };

  const reciveDistricts = (id) => {
    const dataFilter = dataDistricts;
    setstate({
      ...state,
      zipcode: dataFilter.filter((value) => value.id === id)[0].zipcode,
    });
  };

  const insertDataUser = async (value) => {
    const file = [];
    const data = new FormData();
    file.push(
      doc1Ref.current.props.fileList[0] !== undefined
        ? doc1Ref.current.props.fileList[0]
        : "",
      doc2Ref.current.props.fileList[0] !== undefined
        ? doc2Ref.current.props.fileList[0]
        : "",
      doc3Ref.current.props.fileList[0] !== undefined
        ? doc3Ref.current.props.fileList[0]
        : "",
      doc4Ref.current.props.fileList[0] !== undefined
        ? doc4Ref.current.props.fileList[0]
        : ""
    );
    if (file[0] !== "" && file[1] !== "") {
      for (let x = 0; x < file.length; x++) {
        if (file[x].status) {
          if (file[x].status === "error") {
            error(
              "นามสกุลไฟล์ไม่ถูกต้อง หรือเกิดข้อผิดพลาดในการอัพโหลด! ,เลือกไฟล์ใหม่"
            );
            return;
          }
          data.append(
            "file",
            file[x].originFileObj,
            `${Date.now() + "-" + x}.${
              file[x].type === "image/jpeg" ? "jpg" : "png"
            }`
          );
        }
      }
      const res = await insertData(value);

      if (res.success && res.status) {
        const resImg = await upload(data, res.data);

        if (resImg.success && resImg.status) {
          const receiver = fromRef.current.getFieldValue("email");
          const subject = "แจ้งการสมัครสมาชิก";
          const body = `<p>เรียน คุณ${
            resImg.data.name + " " + resImg.data.lastname
          }</p></br>&nbsp&nbsp&nbsp&nbspบริษัทฯ ขอขอบคุณสำหรับการสมัครสมาชิกของท่าน &nbspระบบได้รับข้อมูลการสมัครสมาชิกของท่านเรียบร้อยแล้ว โปรดรอการแจ้งผล ทาง Email ที่ได้ลงทะเบียนไว้หลังตรวจสอบข้อมูลเสร็จสิ้น</br><p style="color:blue">ขอแสดงความนับถือ<br>บริษัท แอ็ดด้า ฟุตแวร์(ไทยเเลนด์) จำกัด<br>โทรศัพฑ์: 0-2416-0026 ต่อ 284<br>e-mail: support@adda.co.th<p>`;
          const res = await sendMail(receiver, subject, body);
          openNotification();
        } else {
          console.log(resImg);
          error(resImg.message);
        }
      } else {
        setstate({
          ...state,
          status: "error",
          message: "ชื่อผู้ใช้งานซ้ำ!",
          inputemail: res.data,
        });
        window.scrollTo({ top: 0, behavior: "smooth" });
      }
    } else {
      // ไฟล์ 1 - 2 ไม่ครบ
      error("โปรดเเนบสำเนาสำเนาทะเบียนบ้าน เเละสำเนาบัตรประชาชน!");
      return;
    }
  };

  const error = (massage) => {
    msg.error({
      content: massage,
      duration: 10,
    });
  };

  const setValue = () => {
    setstate({
      ...state,
      status: null,
      message: null,
    });
  };

  const close = () => {
    router.push("/");
  };

  const openNotification = () => {
    const key = `open${Date.now()}`;
    const btn = (
      <Button
        type="primary"
        size="small"
        onClick={() => {
          router.push("/"), notification.close(key);
        }}
      >
        ตกลง
      </Button>
    );
    notification.open({
      message: "เเจ้งเตือนระบบ",
      description: "สมัครสมาชิคเรียบร้อย, รออนุมัญติเอกสาร",
      btn,
      key,
      onClose: close,
    });
  };

  const key = "updatable";

  const openMessage = () => {
    message.loading({ content: "Loading...", key });
    setTimeout(() => {
      message
        .success({
          content: "สมัครสมาชิคเรียนร้อย! ,รออนุมัญิเอกสาร",
          key,
          duration: 4,
        })
        .then(() => router.push("/"));
    }, 1500);
  };

  const style = {
    height: 40,
    width: 100,
    lineHeight: "40px",
    borderRadius: 4,
    backgroundColor: "#1088e9",
    color: "#fff",
    textAlign: "center",
    fontSize: 14,
  };

  const { provinces } = props;
  const {
    dataAmphures,
    dataDistricts,
    zipcode,
    status,
    message,
    inputemail,
  } = state;
  return (
    <Container className="animate__animated animate__fadeIn ">
      <Row>
        <Col md={{ size: 8, offset: 2 }}>
          <h1>ลงทะเบียน</h1>
          <ComponentFormdata
            provinces={provinces}
            amphures={dataAmphures}
            districts={dataDistricts}
            zipcode={zipcode}
            status={status}
            message={message}
            inputemail={inputemail}
            fromRef={fromRef}
            reciveProvinces={reciveProvinces}
            reciveAmpures={reciveAmpures}
            reciveDistricts={reciveDistricts}
            insertDataUser={insertDataUser}
            setValue={setValue}
          />
          <Row>
            <Col md="6">
              <h2 className="mb-0">เเนบสำเนา</h2>
              <strong style={{ color: "red" }} className="ml-1">
                (นามสกุลไฟล์ต้องเป็น .PNG/.JPG)
              </strong>
              <ComponentFormdoc
                doc1Ref={doc1Ref}
                doc2Ref={doc2Ref}
                doc3Ref={doc3Ref}
                doc4Ref={doc4Ref}
              />
            </Col>
            <Col md="6"></Col>
          </Row>
          <Button
            type="primary"
            danger
            style={{ width: "100%", marginTop: 35 }}
            onClick={() => {
              fromRef.current.submit();
              // fromRef2.current.submit();
            }}
          >
            ลงทะเบียน
          </Button>
        </Col>
      </Row>
      <BackTop>
        <div style={style}>ขึ้นด้านบน</div>
      </BackTop>
    </Container>
  );
}
Register.getInitialProps = async (ctx) => {
  const res = await qureyDataThailand();
  const data = await res.map((value, index) => {
    return {
      id: value.id,
      code: value.code,
      value: value.name_th,
    };
  });
  return { provinces: data };
};
export default Register;
