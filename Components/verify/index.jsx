import React, { useState, useEffect } from "react";
import { Modal, Button, Tooltip } from "antd";
import { Row, Col } from "reactstrap";
import { getDoc } from "../../hooks/querysql";

const path = "http://10.32.0.14:83/doc/";

const CardImage = ({ item }) => {
  console.log(item);
  return (
    <>
      <div
        className="verify m-1"
        // onClick={() => this.purchaseProduct(index)}
      >
        <Row>
          <Col md="4">
            <img
              className="verify-img"
              src={path + item.doc1}
              alt="Image Not Found"
            />
          </Col>
          <Col md="8">
            <div className="text-left p-2">
              <div className="d-flex align-items-center mb-2">
                <b>สำเนาบัตรประชาชน</b>
              </div>
              <div className="d-flex align-items-center mb-2">
                <b>สถานะ :</b>
                <label className="product-desc mb-0 ml-1">
                  {item.appv_doc1 ? "อนุมัติ" : "ไม่อนุมัติ"}
                </label>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      <div
        className="verify m-1"
        // onClick={() => this.purchaseProduct(index)}
      >
        <Row>
          <Col md="4">
            <img
              className="verify-img"
              src={path + item.doc2}
              alt="Image Not Found"
            />
          </Col>
          <Col md="8">
            <div className="text-left p-2">
              <div className="d-flex align-items-center mb-2">
                <b>สำเนาบัตรประชาชน</b>
              </div>
              <div className="d-flex align-items-center mb-2">
                <b>สถานะ :</b>
                <label className="product-desc mb-0 ml-1">
                  {item.appv_doc2 ? "อนุมัติ" : "ไม่อนุมัติ"}
                </label>
              </div>
            </div>
          </Col>
        </Row>
      </div>
      {item.doc3 && (
        <div
          className="verify m-1"
          // onClick={() => this.purchaseProduct(index)}
        >
          <Row>
            <Col md="4">
              <img
                className="verify-img"
                src={path + item.doc3}
                alt="Image Not Found"
              />
            </Col>
            <Col md="8">
              <div className="text-left p-2">
                <div className="d-flex align-items-center mb-2">
                  <b>สำเนาบัตรประชาชน</b>
                </div>
                <div className="d-flex align-items-center mb-2">
                  <b>สถานะ :</b>
                  <label className="product-desc mb-0 ml-1">
                    {item.appv_doc3 ? "อนุมัติ" : "ไม่อนุมัติ"}
                  </label>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      )}
      {item.doc4 && (
        <div
          className="verify m-1"
          // onClick={() => this.purchaseProduct(index)}
        >
          <Row>
            <Col md="4">
              <img
                className="verify-img"
                src={path + item.doc4}
                alt="Image Not Found"
              />
            </Col>
            <Col md="8">
              {" "}
              <div className="text-left p-2">
                <div className="d-flex align-items-center mb-2">
                  <b>สำเนาบัตรประชาชน</b>
                </div>
                <div className="d-flex align-items-center mb-2">
                  <b>สถานะ :</b>
                  <label className="product-desc mb-0 ml-1">
                    {item.appv_doc4 ? "อนุมัติ" : "ไม่อนุมัติ"}
                  </label>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      )}
    </>
  );
};

function Index({ visible, handleCancel, email }) {
  const initialState = {
    dataImages: [],
  };
  const [state, setState] = useState(initialState);

  const getData = async (email) => {
    try {
      const res = await getDoc(email);

      if (!res.success && !res.status) return console.log(res);
      const data = await res.data;
      setState({
        ...state,
        dataImages: data,
      });
    } catch (err) {
      // console.error(err);
      
    }
  };
  useEffect(() => {
    getData(email);
  }, [visible]);

  const { dataImages } = state;
  return (
    <div>
      <Modal
        width={750}
        title="ตรวจสอบการอนุมัติ"
        visible={visible}
        // onOk={this.handleOk}
        onCancel={handleCancel}
        footer={[
          <Button key="submit" type="primary" onClick={handleCancel}>
            ย้อนกลับ
          </Button>,
        ]}
      >
        {dataImages &&
          dataImages.map((item, index) => {
            return (
              <div key={index}>
                <CardImage item={item} />
              </div>
            );
          })}
      </Modal>
    </div>
  );
}

export default Index;
