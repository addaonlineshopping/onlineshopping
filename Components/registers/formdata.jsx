import React, { useState, useEffect } from "react";
import NumberFormat from "react-number-format";
import { Row, Col } from "reactstrap";
import moment from "moment";
import {
  Input,
  Button,
  Form,
  Select,
  AutoComplete,
  Tooltip,
  Collapse,
  Checkbox,
} from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";

const { Panel } = Collapse;
const month = [
  { num: "01", name: "มกราคม" },
  { num: "02", name: "กุมภาพันธ์" },
  { num: "03", name: "มีนาคม" },
  { num: "04", name: "เมษายน" },
  { num: "05", name: "พฤษภาคม" },
  { num: "06", name: "มิถุนายน" },
  { num: "07", name: "กรกฎาคม" },
  { num: "08", name: "สิงหาคม" },
  { num: "09", name: "กันยายน" },
  { num: "10", name: "ตุลาคม" },
  { num: "11", name: "พฤศจิกายน" },
  { num: "12", name: "ธันวาคม" },
];
const { Option } = Select;
function Formdata(props) {
  const [form] = Form.useForm();
  const initialState = {
    key: 0,
  };
  const [state, setState] = useState(initialState);
  const onFinish = (fieldsValue) => {
    const a = `${parseInt(fieldsValue["year"]) - 543}-${fieldsValue["month"]}-${
      fieldsValue["day"]
    }`;

    const values = {
      ...fieldsValue,
      date: moment(a).utc().format("YYYY-MM-DD"),
    };

    insertDataUser(values);
  };

  const handleChangePro = (value, row) => reciveProvinces(row.id);
  const handleChangeAmp = (value, row) => reciveAmpures(row.id);
  const handleChangeDis = (value, row) => reciveDistricts(row.id);

  const genExtra = () => {
    return (
      <p style={{ marginBottom: 0, marginLeft: 10 }}>
        <Checkbox
          // disabled={this.state.disabled}
          onChange={
            (e) => {
              if (e.target.checked) {
                setState({
                  key: 1,
                });
              } else {
                setState({
                  key: 0,
                });
              }
            }
            // setState({
            //   key: 1,
            // })
          }
        >
          มีใบกำกับภาษี
        </Checkbox>
      </p>
    );
  };

  const {
    provinces,
    amphures,
    districts,
    zipcode,
    reciveProvinces,
    reciveAmpures,
    reciveDistricts,
    insertDataUser,
    fromRef,
    status,
    message,
    setValue,
  } = props;

  useEffect(() => {
    fromRef.current.setFieldsValue({
      zipcode: zipcode ? zipcode : "",
    });
  }, [zipcode]);

  useEffect(() => {
    fromRef.current.setFieldsValue({
      amphur: "",
      tambol: "",
      zipcode: "",
    });
  }, [amphures]);

  useEffect(() => {
    fromRef.current.setFieldsValue({
      tambol: "",
      zipcode: "",
    });
  }, [districts]);

  return (
    <Form
      form={form}
      layout="vertical"
      ref={fromRef}
      onFinish={onFinish}
      onFinishFailed={() => fromRef.current.validateFields(["username"])}
      initialValues={{
        prefix: "นาย",
      }}
    >
      <Row>
        <Col md="12">
          {status === null ? (
            <Form.Item
              name="email"
              label="อีเมล"
              rules={[
                {
                  required: true,
                  type: "email",
                  message: "กรอกอีเมลให้ถูกต้อง!",
                },
              ]}
            >
              <Input
                placeholder="example@email.com."
                autoComplete="off"
                allowClear={true}
                onChange={(e) => setValue()}
              />
            </Form.Item>
          ) : (
            <Form.Item
              name="email"
              label="อีเมล"
              validateStatus={status}
              help={message}
              rules={[
                {
                  required: true,
                  type: "email",
                },
              ]}
            >
              <Input
                placeholder="example@email.com."
                autoComplete="off"
                allowClear={true}
              />
            </Form.Item>
          )}
        </Col>
        <Col md="6">
          <Form.Item
            name="password"
            label="รหัสผ่าน"
            rules={[
              {
                required: true,
                message: "กรุณากรอกรหัสผ่าน!",
              },
              {
                min: 5,
                message: "รหัสผ่านต้องมีความยาวเกิน 5 ตัวอักษร!",
              },
              {
                pattern: /^[A-Z\a-z\0-9\-]*$/,
                message: "รหัสผ่านต้องเป็นภาษาอังกฤษ หรือตัวเลขเท่านั้น",
              },
            ]}
          >
            <Input.Password placeholder="..." allowClear={true} />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="confirm"
            label="ยืนยันรหัสผ่าน"
            dependencies={["password"]}
            rules={[
              {
                required: true,
                message: "กรุณายืนยันรหัสผ่าน!",
              },
              ({ getFieldValue }) => ({
                validator(rule, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }

                  return Promise.reject("รหัสไม่ตรงกัน!");
                },
              }),
            ]}
          >
            <Input.Password placeholder="..." allowClear={true} />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            label="ชื่อ"
            name="firstname"
            rules={[{ required: true, message: "กรุณากรอกชื่อ!" }]}
          >
            <Input
              autoComplete="off"
              placeholder="..."
              addonBefore={
                <Form.Item name="prefix" noStyle>
                  <Select style={{ width: 90 }}>
                    <Option value="นาย">นาย</Option>
                    <Option value="นาง">นาง</Option>
                    <Option value="นางสาว">นางสาว</Option>
                  </Select>
                </Form.Item>
              }
              style={{ width: "100%" }}
            />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="lastname"
            label="นามสกุล"
            rules={[
              {
                required: true,
                message: "กรุณากรอกนานสกุล!",
              },
            ]}
          >
            <Input
              autoComplete="off"
              maxLength={50}
              placeholder="..."
              allowClear={true}
            />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="idcard"
            label="เลขบัตรประชาชน"
            rules={[
              {
                required: true,
                message: "กรอกเลขบัตรประชาชน!",
              },
              () => ({
                validator(rule, value) {
                  if (!value || value.replace(/\s/g, "").length === 13) {
                    return Promise.resolve();
                  }

                  return Promise.reject("กรอกเลขบัตรประชาชนไม่ครบ 13 หลัก");
                },
              }),
            ]}
          >
            <NumberFormat
              autoComplete="off"
              format="# #### ##### ## #"
              customInput={Input}
              allowClear={true}
              placeholder="# #### ##### ## #"
            />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item name="date" label="วัน/เดือน/ปีเกิด (ปี พ.ศ.)">
            <Input.Group compact>
              <Form.Item
                name="day"
                noStyle
                rules={[
                  { required: true, message: "เลือกวัน!" },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || parseInt(getFieldValue("day")) <= 31) {
                        return Promise.resolve();
                      }

                      return Promise.reject("กรอกวันที่ให้ถูกต้อง!");
                    },
                  }),
                ]}
              >
                <Input
                  style={{ width: "20%", height: 32 }}
                  placeholder="วัน"
                  autoComplete="off"
                  className="text-center"
                  maxLength={2}
                />
              </Form.Item>
              <Form.Item
                name="month"
                noStyle
                rules={[{ required: true, message: "เลือกเดือน!" }]}
              >
                <Select
                  style={{ width: "60%" }}
                  placeholder="เดือน"
                  className="text-center"
                >
                  {month.map((item, index) => {
                    return (
                      <Option key={index} value={item.num}>
                        {item.name}
                      </Option>
                    );
                  })}
                </Select>
              </Form.Item>
              <Form.Item
                name="year"
                noStyle
                rules={[
                  { required: true, message: "เลือกปี!" },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (
                        !value ||
                        (parseInt(getFieldValue("year")) >= 1900 &&
                          parseInt(getFieldValue("year")) <=
                            new Date().getFullYear() + 543)
                      ) {
                        return Promise.resolve();
                      }
                      return Promise.reject("กรอกปีที่ให้ถูกต้อง!");
                    },
                  }),
                ]}
              >
                <Input
                  maxLength={4}
                  style={{ width: "20%", height: 32 }}
                  placeholder="ปี"
                  autoComplete="off"
                  className="text-center"
                />
              </Form.Item>
            </Input.Group>
          </Form.Item>
        </Col>

        <Col md="12">
          <Form.Item name="company" label="ชื่อร้าน (ถ้ามี)">
            <Input autoComplete="off" maxLength={50} allowClear={true} />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="address"
            label="ที่อยู่ปัจจุบัน"
            rules={[
              {
                required: true,
                message: "กรอกที่อยู่ปัจจุบัน!",
              },
            ]}
          >
            <Input autoComplete="off" maxLength={50} allowClear={true} />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="province"
            label="จังหวัด"
            rules={[
              {
                required: true,
                message: "เลือกจังหวัด!",
              },
            ]}
          >
            {provinces && (
              <AutoComplete
                style={{
                  width: "100%",
                }}
                children={
                  <Input
                    allowClear={true}
                    suffix={
                      <Tooltip title="พิมพ์ชื่อจังหวัด เพื่อค้นหา">
                        <InfoCircleOutlined
                          style={{ color: "rgba(0,0,0,.45)" }}
                        />
                      </Tooltip>
                    }
                  />
                }
                options={provinces}
                onSelect={handleChangePro}
                placeholder="ค้นหาจังหวัด"
                filterOption={(inputValue, option) =>
                  option.value
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
              />
            )}
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="amphur"
            label="อำเภอ / เขต"
            rules={[
              {
                required: true,
                message: "เลือกอำเภอ / เขต!",
              },
            ]}
          >
            {amphures && (
              <AutoComplete
                style={{
                  width: "100%",
                }}
                disabled={amphures.length === 0 ? true : false}
                children={
                  <Input
                    allowClear={true}
                    suffix={
                      <Tooltip
                        title={
                          amphures.length === 0
                            ? "ยังไม่ได้ระบุจังหวัด!"
                            : "พิมพ์ชื่ออำเภอหรือเขต เพื่อค้นหา"
                        }
                      >
                        <InfoCircleOutlined
                          style={{ color: "rgba(0,0,0,.45)" }}
                        />
                      </Tooltip>
                    }
                  />
                }
                options={amphures}
                onSelect={handleChangeAmp}
                placeholder="ค้นหาเลือกอำเภอ / เขต"
                filterOption={(inputValue, option) =>
                  option.value
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
              />
            )}
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="tambol"
            label="ตำบล / แขวง"
            rules={[
              {
                required: true,
                message: "เลือกตำบล / แขวง!",
              },
            ]}
          >
            {districts && (
              <AutoComplete
                style={{
                  width: "100%",
                }}
                disabled={districts.length === 0 ? true : false}
                children={
                  <Input
                    suffix={
                      <Tooltip
                        title={
                          districts.length === 0
                            ? "ยังไม่ได้ระบุจังหวัด!"
                            : "พิมพ์ชื่ออำเภอหรือเขต เพื่อค้นหา"
                        }
                      >
                        <InfoCircleOutlined
                          style={{ color: "rgba(0,0,0,.45)" }}
                        />
                      </Tooltip>
                    }
                  />
                }
                options={districts}
                onSelect={handleChangeDis}
                placeholder="ค้นหาเลือกอำเภอ / เขต"
                filterOption={(inputValue, option) =>
                  option.value
                    .toUpperCase()
                    .indexOf(inputValue.toUpperCase()) !== -1
                }
              />
            )}
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            label="รหัสไปรษณีย์"
            name="zipcode"
            rules={[
              {
                required: true,
                message: "กรอกรหัสไปรษณีย์!",
              },
              () => ({
                validator(rule, value) {
                  if (!value || value.toString().length === 5) {
                    return Promise.resolve();
                  }

                  return Promise.reject("กรอกรหัสไปรษณีย์ไม่ครบ 5 หลัก");
                },
              }),
            ]}
          >
            <NumberFormat
              format="#####"
              autoComplete="off"
              customInput={Input}
              allowClear={true}
              placeholder="..."
            />
          </Form.Item>
        </Col>
        <Col md="6">
          <Form.Item
            name="phone"
            label="เบอร์โทรที่ติดต่อได้"
            rules={[
              {
                required: true,
                message: "กรอกเบอร์โทรศัพท์!",
              },
              () => ({
                validator(rule, value) {
                  if (!value || value.replace(/\s/g, "").length === 10) {
                    return Promise.resolve();
                  }

                  return Promise.reject("กรอกเบอร์โทรศัพท์ไม่ครบ 10 หลัก");
                },
              }),
            ]}
          >
            <NumberFormat
              autoComplete="off"
              format="### #### ###"
              customInput={Input}
              allowClear={true}
              placeholder="08# #### ###"
            />
          </Form.Item>
        </Col>
        <Col md="12" className="mb-3">
          <Collapse accordion activeKey={state.key} className="invoice-call">
            <Panel header="ข้อมูลใบกำกับภาษี" key="1" extra={genExtra()}>
              <Row>
                <Col md="6">
                  <Form.Item
                    name="invoice"
                    label="เลขที่ใบกำกับภาษี"
                    rules={[
                      {
                        required: state.key === 1 ? true : false,
                        message: "กรอกเลขที่ใบกำกับภาษี!",
                      },
                    ]}
                  >
                    <Input
                      placeholder="..."
                      autoComplete="off"
                      allowClear={true}
                    />
                  </Form.Item>
                </Col>
                <Col md="6">
                  <Form.Item
                    name="phoneInvoice"
                    label="เบอร์โทรที่ติดต่อได้"
                    rules={[
                      {
                        required: state.key === 1 ? true : false,
                        message: "กรอกเบอร์โทรศัพท์!",
                      },
                      () => ({
                        validator(rule, value) {
                          if (
                            !value ||
                            value.replace(/\s/g, "").length === 10
                          ) {
                            return Promise.resolve();
                          }

                          return Promise.reject(
                            "กรอกเบอร์โทรศัพท์ไม่ครบ 10 หลัก"
                          );
                        },
                      }),
                    ]}
                  >
                    <NumberFormat
                      autoComplete="off"
                      format="### #### ###"
                      customInput={Input}
                      allowClear={true}
                      placeholder="08# #### ###"
                    />
                  </Form.Item>
                </Col>
                <Col md="12">
                  <Form.Item
                    name="addressInvoice"
                    label="ที่อยู่"
                    rules={[
                      {
                        required: state.key === 1 ? true : false,
                        message: "กรอกที่อยู่!",
                      },
                    ]}
                  >
                    <Input
                      placeholder="..."
                      autoComplete="off"
                      allowClear={true}
                    />
                  </Form.Item>
                </Col>
                <Col md="6">
                  <Form.Item
                    name="provinceInvoice"
                    label="เลือกจังหวัด"
                    rules={[
                      {
                        required: state.key === 1 ? true : false,
                        message: "กรอกเลขที่ใบกำกับภาษี!",
                      },
                    ]}
                  >
                    <AutoComplete
                      style={{
                        width: "100%",
                      }}
                      children={
                        <Input
                          allowClear={true}
                          suffix={
                            <Tooltip title="พิมพ์ชื่อจังหวัด เพื่อค้นหา">
                              <InfoCircleOutlined
                                style={{ color: "rgba(0,0,0,.45)" }}
                              />
                            </Tooltip>
                          }
                        />
                      }
                      // options={provinces}
                      // onSelect={handleChangePro}
                      // placeholder="ค้นหาจังหวัด"
                      // filterOption={(inputValue, option) =>
                      //   option.value
                      //     .toUpperCase()
                      //     .indexOf(inputValue.toUpperCase()) !== -1
                      // }
                    />
                  </Form.Item>
                </Col>
                <Col md="6">
                  <Form.Item
                    name="amphurInvoice"
                    label="อำเภอ / เขต"
                    rules={[
                      {
                        required: state.key === 1 ? true : false,
                        message: "เลือกอำเภอ / เขต!",
                      },
                    ]}
                  >
                    <AutoComplete
                      style={{
                        width: "100%",
                      }}
                      children={
                        <Input
                          allowClear={true}
                          suffix={
                            <Tooltip title="พิมพ์ชื่ออำเภอหรือเขต เพื่อค้นหา">
                              <InfoCircleOutlined
                                style={{ color: "rgba(0,0,0,.45)" }}
                              />
                            </Tooltip>
                          }
                        />
                      }
                      // options={provinces}
                      // onSelect={handleChangePro}
                      // placeholder="ค้นหาจังหวัด"
                      // filterOption={(inputValue, option) =>
                      //   option.value
                      //     .toUpperCase()
                      //     .indexOf(inputValue.toUpperCase()) !== -1
                      // }
                    />
                  </Form.Item>
                </Col>
                <Col md="6">
                  <Form.Item
                    name="tambolInvoice"
                    label="ตำบล / แขวง"
                    rules={[
                      {
                        required: state.key === 1 ? true : false,
                        message: "เลือกตำบล / แขวง!",
                      },
                    ]}
                  >
                    <AutoComplete
                      style={{
                        width: "100%",
                      }}
                      children={
                        <Input
                          allowClear={true}
                          suffix={
                            <Tooltip title="พิมพ์ชื่ออำเภอหรือเขต เพื่อค้นหา">
                              <InfoCircleOutlined
                                style={{ color: "rgba(0,0,0,.45)" }}
                              />
                            </Tooltip>
                          }
                        />
                      }
                      // options={provinces}
                      // onSelect={handleChangePro}
                      // placeholder="ค้นหาจังหวัด"
                      // filterOption={(inputValue, option) =>
                      //   option.value
                      //     .toUpperCase()
                      //     .indexOf(inputValue.toUpperCase()) !== -1
                      // }
                    />
                  </Form.Item>
                </Col>
                <Col md="6">
                  <Form.Item
                    label="รหัสไปรษณีย์"
                    name="zipcodeInvoice"
                    rules={[
                      {
                        required: state.key === 1 ? true : false,
                        message: "กรอกรหัสไปรษณีย์!",
                      },
                      () => ({
                        validator(rule, value) {
                          if (!value || value.toString().length === 5) {
                            return Promise.resolve();
                          }

                          return Promise.reject(
                            "กรอกรหัสไปรษณีย์ไม่ครบ 5 หลัก"
                          );
                        },
                      }),
                    ]}
                  >
                    <NumberFormat
                      format="#####"
                      autoComplete="off"
                      customInput={Input}
                      allowClear={true}
                      placeholder="..."
                    />
                  </Form.Item>
                </Col>
              </Row>
            </Panel>
          </Collapse>
        </Col>
        <Col md="6" className="d-flex align-items-center">
          <Button htmlType="submit" style={{ display: "none", width: "100%" }}>
            ลงทะเบียน
          </Button>
        </Col>
      </Row>
    </Form>
  );
}

export default Formdata;
