import React, { useState } from "react";

import { Col } from "reactstrap";
import { Upload, message, Button } from "antd";
import { UploadOutlined } from "@ant-design/icons";

function Formdoc({ doc1Ref,doc2Ref,doc3Ref,doc4Ref }) {
  const [stateFile, setFile] = useState({
    file1: [],
    file2: [],
    file3: [],
    file4: [],
  });

  const onChange = (value) => ({ file, fileList }) => {
    let fileLists = [...fileList];
    fileLists = fileLists.slice(-1);
    fileLists = fileLists.map((file, index) => {
      if (!file.status) {
        return {
          ...fileLists[index],
          status: "error",
        };
      } else {
        if (file.response) {
          return {
            ...fileLists[index],
            status: "done",
            url: file.response.url,
          };
        } else {
          return {
            ...fileLists[index],
            status: "done",
            name: file.originFileObj.name,
          };
        }
      }
    });

    setFile({
      ...stateFile,
      [value]: fileLists,
    });
  };

  function beforeUpload(file) {
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }

    return isJpgOrPng;
  }

  const { file1, file2, file3, file4 } = stateFile;
  return (
    <>
      <Col md="12" style={{ marginTop: "3%" }}>
        <p
          style={{
            color: "black",
            fontSize: "14px",
            fontWeight: "bolder",
          }}
        >
          1. สำเนาทะเบียนบ้าน
        </p>
        <Upload
          ref={doc1Ref}
          fileList={file1}
          beforeUpload={beforeUpload}
          onChange={onChange("file1")}
        >
          <Button>
            <UploadOutlined /> Upload
          </Button>
        </Upload>
      </Col>
      <Col md="12" style={{ marginTop: "3%" }}>
        <p
          style={{
            color: "black",
            fontSize: "14px",
            fontWeight: "bolder",
          }}
        >
          2. สำเนาบัตรประชาชน
        </p>
        <Upload
          ref={doc2Ref}
          fileList={file2}
          beforeUpload={beforeUpload}
          onChange={onChange("file2")}
        >
          <Button>
            <UploadOutlined /> Upload
          </Button>
        </Upload>
      </Col>
      <Col md="12" style={{ marginTop: "3%" }}>
        <p
          style={{
            color: "black",
            fontSize: "14px",
            fontWeight: "bolder",
          }}
        >
          3. สำเนาทะเบียนภาณิชย์ (ถ้ามี)
        </p>
        <Upload
          ref={doc3Ref}
          fileList={file3}
          beforeUpload={beforeUpload}
          onChange={onChange("file3")}
        >
          <Button>
            <UploadOutlined /> Upload
          </Button>
        </Upload>
      </Col>
      <Col md="12" style={{ marginTop: "3%" }}>
        <p
          style={{
            color: "black",
            fontSize: "14px",
            fontWeight: "bolder",
          }}
        >
          4. รูปถ่ายหน้าร้าน (ถ้ามี)
        </p>
        <Upload
          ref={doc4Ref}
          fileList={file4}
          beforeUpload={beforeUpload}
          onChange={onChange("file4")}
        >
          <Button>
            <UploadOutlined /> Upload
          </Button>
        </Upload>
      </Col>
    </>
  );
}

export default Formdoc;
