import React, { useState, useContext } from "react";
import Link from "next/link";
import {
  IoMdLocate,
  IoMdPersonAdd,
  IoIosLock,
  IoLogoWindows,
  IoMdMenu,
} from "react-icons/io";
import { Container, Row, Col } from "reactstrap";
import { Menu, Dropdown, Button, Badge } from "antd";

const dataMenu1 = [
  { name: "ตรวจสอบสถานะ", link: "/verifyuser", icon: <IoMdLocate className="mr-1" /> },
  {
    name: "ลงทะเบียน",
    // link: "/dealer_public/register",
    link: "/register",
    icon: <IoMdPersonAdd className="mr-1" />,
  },
  { name: "เข้าสู่ระบบ", link: "/", icon: <IoIosLock className="mr-1" /> },
];

const dataMenu2 = [
  { name: "หน้าแรก", link: "/" },
  { name: "สินค้า", link: "/" },
  { name: "วิธีสั่งซื้อ", link: "/" },
  { name: "แจ้งชำระเงิน", link: "/" },
  { name: "ประวัติการสั่งซื้อ", link: "/" },
  { name: "เกี่ยวกับเรา", link: "/" },
];

const OptionMax = () => {
  return (
    <div className="display-max">
      {dataMenu1.map((value, index) => {
        return index + 1 === dataMenu1.length ? (
          <Link key={index} href={value.link} >
            <a style={{color:"#ffffff"}}>
              {value.icon}
              {value.name}
            </a>
          </Link>
        ) : (
          <Link key={index} href={value.link}>
            <a style={{color:"#ffffff"}} className="mr-4">
              {value.icon}
              {value.name}
            </a>
          </Link>
        );
      })}
    </div>
  );
};

const menu1 = (
  <Menu>
    {dataMenu1.map((value, index) => {
      return (
        <Menu.Item key={index}>
          <a className="ant-dropdown-link">
            {value.icon}
            {value.name}
          </a>
        </Menu.Item>
      );
    })}
  </Menu>
);

const OptionMin = () => {
  return (
    <div className="display-min">
      <Dropdown overlay={menu1} placement="bottomLeft">
        <a className="ant-dropdown-link">
          <IoLogoWindows className="mr-1" />
          เมนูผู้ใช้งาน
        </a>
      </Dropdown>
    </div>
  );
};

const menu2 = (
  <Menu>
    {dataMenu2.map((value, index) => {
      return (
        <Menu.Item key={index}>
          <a className="ant-dropdown-link">{value.name}</a>
        </Menu.Item>
      );
    })}
  </Menu>
);

const SemiMin = () => {
  return (
    <Col
      xs="7"
      className="d-flex align-items-center justify-content-end display-min"
    >
      <Dropdown overlay={menu2} placement="bottomLeft">
        <a className="ant-dropdown-link">
          <IoMdMenu className="mr-1" />
          รายการ
        </a>
      </Dropdown>
    </Col>
  );
};

const SemiMax = () => {
  return (
    <Col
      md="9"
      className="d-flex align-items-center justify-content-end display-max pr-0"
    >
      <div className="display-max">
        {dataMenu2.map((value, index) => {
          return index + 1 === dataMenu2.length ? (
            <a key={index} className="ant-dropdown-link hovers">
              {value.name}
            </a>
          ) : (
            <a key={index} className="ant-dropdown-link hovers mr-4 ">
              {value.name}
            </a>
          );
        })}
      </div>
    </Col>
  );
};

const SemiNavbar = () => {
  return (
    <Container>
      <Row style={{ paddingRight: 15 }}>
        <Col xs="5" md="3">
          <img alt="example" src={`http://112.121.138.46/dealer_public/images/img01.jpg`} height="50px" />
        </Col>
        <SemiMin />
        <SemiMax />
      </Row>
    </Container>
  );
};

const HeaderNavbar = () => {
  return (
    <div className="style-hnav">
      <Container>
        <Row>
          <Col xs="5" sm="5" md="5">
            ADDA ขายส่งรองเท้า
          </Col>
          <Col
            xs="7"
            sm="7"
            md="7"
            className="d-flex align-items-center layout "
          >
            <OptionMax />
            <OptionMin />
          </Col>
        </Row>
      </Container>
    </div>
  );
};

function Header() {
  return (
    <>
      <HeaderNavbar />
      <SemiNavbar />
      <hr className="m-0" />
    </>
  );
}

export default Header;
