import React from "react";
import Header from "./header";
import Footer from "./footer";
import { Container, Row, Col } from "reactstrap";
function Layout(props) {
  return (
    <>
      <Header />
      <Container className="mt-3 mb-3">{props.children}</Container>
      <Footer />
    </>
  );
}

export default Layout;
